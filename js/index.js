/* ==================== Home page js =========================== */
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(()=>{
    onPageLoad();
   })
   /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
   // hàm sự kiện được thực thi khi tải trang
   function onPageLoad(){
       console.log('%cTrang được tải','color:green')
       var vDivPopularElement = '#div-row-popular-courses';
       var vDivTrendingElement = '#div-row-trending-courses';
       var vPopularCourses = filterPopularCourses(gCoursesDB.courses);
       var vTrendingCourses = filterTrendingCourses(gCoursesDB.courses);
       console.log(vPopularCourses);
       console.log(vPopularCourses);
       getCoursesToHTML(vDivPopularElement,vPopularCourses);
       getCoursesToHTML(vDivTrendingElement,vTrendingCourses);
   }
   /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
   // hàm lọc mảng chứa khóa học Popular
   function filterPopularCourses(paramCousesArr){
       var vPopularCourses = paramCousesArr.filter(course => course.isPopular === true);
       return vPopularCourses;
   }
   // hàm lọc mảng chứa khóa học Trending
   function filterTrendingCourses(paramCousesArr){
       var vTrendingCourses = paramCousesArr.filter(course => course.isTrending === true);
       return vTrendingCourses;
   }
   /* Hàm tải thông tin khóa học nên web  
    * đầu vào thẻ div chứa nội dung các khóa học như Popular Trending, mảng chứa các khóa học
    */ 
   function getCoursesToHTML(paramElement,paramCousesArr){
       var vDivPopularElement = '#div-row-popular-courses';
       var vDivTrendingElement = '#div-row-trending-courses';
       $(paramElement).html('');
       paramCousesArr.map((course,index)=>{
           var vContent = `<div class="col-3">
           <div class="card">
               <img class="rounded-top" src=${course.coverImage}>
               <div class="card-body">
                   <a class="card-title my-2 txt-bold text-sm" href="#">${course.courseName}</a>
                   <p class="card-text mt-2"><i class="far fa-clock"></i> ${course.duration} ${course.level}</p>
                   <p class="my-2"><b>${course.discountPrice}$</b><del> ${course.price}$</del></p>
               </div>
               <div class="card-footer">
                   <img style="width: 15%;" class="rounded-circle float-left" src=${course.teacherPhoto} alt="">
                   <p class="pl-5">${course.teacherName}</p>
                   <i class="far fa-bookmark float-right fa-lg"></i>
               </div>
               </div>
           </div>`;
           
           $(vContent).appendTo(paramElement);
       });
       if (paramElement==vDivPopularElement){
           console.log("Các khóa học Popular đã được thêm thành công");
       }
       else if(paramElement==vDivTrendingElement){
           console.log("Các khóa học Trending đã được thêm thành công")
       }
       
   }
// Khi load trang
$(document).ready(function () {
    onPageLoading()
    // Gán sự kiện cho nút thêm mới
    $('#btn-add-course').click(onBtnAddCourse)
    // Gán sự kiện cho nút thêm mới (modal)
    $('#btn-create-course').click(onBtnCreateCourse)
    // Gán sự kiện cho nút edit
    $('#courses-table').on('click', '.btn-edit-course', onBtnEditCourseClick)
    // Gán sự kiện cho nút delete
    $('#courses-table').on('click', '.btn-delete-course', onBtnDeleteCourseClick)
    // Gán sự kiện cho nút cập nhật (modal)
    $('#btn-edit-course').click(onBtnEditCourse)
    // Gán sự kiện cho nút confirm delete course (modal)
    $('#btn-confirm-delete-course').click(onBtnConfirmDeleteCourseClick)
})

// Hàm hiển thị courses khi loading
function onPageLoading() {
    // Định nghĩa datatable
    $('#courses-table').DataTable({
        columns: [
            { data: 'id' },
            { data: 'courseCode' },
            { data: 'courseName' },
            { data: 'price' },
            { data: 'discountPrice' },
            { data: 'duration' },
            { data: 'level' },
            { data: 'coverImage' },
            { data: 'teacherName' },
            { data: 'teacherPhoto' },
            { data: 'isPopular' },
            { data: 'isTrending' },
            { data: 'actions' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: '<i class="far fa-edit btn-edit-course" title="Edit course" style="cursor:pointer;"></i>&nbsp;<i class="fas fa-trash btn-delete-course" title="Delete course" style="color: #333; cursor:pointer;"></i>'
            }
        ]
    })

    // Đổ data vào table
    loadCoursesToTable(gCoursesDB.courses)
}

// Hàm khi bấm nút thêm course
function onBtnAddCourse() {
    var vNextId = getNextId()
    $('#inp-create-course-id').val(vNextId)
    $('#create-course-modal').modal()
}

// Hàm khi bấm nút thêm mới (modal)
function onBtnCreateCourse() {
    // Thu thập dữ liệu
    var vCourseData = getDataFromCreateForm()
    // Kiểm tra dữ liệu
    var vValid = validateData(vCourseData)
    if (vValid) {
        // Thêm khóa học vào db
        gCoursesDB.courses.push(vCourseData)
        loadCoursesToTable(gCoursesDB.courses)
        $('#create-course-modal').modal('hide')
        clearCreateForm()
    }
}

// Hàm khi bấm nút edit course
function onBtnEditCourseClick() {
    $('#edit-course-modal').modal()
    var vTable = $('#courses-table').DataTable()
    var vRowData = vTable.row($(this).parents('tr')).data()
    loadCourseDataToEditForm(vRowData)
}

// Hàm khi bấm nút edit course (modal)
function onBtnEditCourse() {
    // Thu thập dữ liệu
    var vCourseData = getDataFormEditCourse()
    // Kiểm tra dữ liệu
    var vValid = validateData(vCourseData)
    if (vValid) {
        // Xử lí cập nhật course data theo id
        for (var bCourse of gCoursesDB.courses) {
            if (bCourse.id == vCourseData.id) {
                bCourse.courseCode = vCourseData.courseCode
                bCourse.courseName = vCourseData.courseName
                bCourse.price = vCourseData.price
                bCourse.discountPrice = vCourseData.discountPrice
                bCourse.duration = vCourseData.duration
                bCourse.level = vCourseData.level
                bCourse.coverImage = vCourseData.coverImage
                bCourse.teacherName = vCourseData.teacherName
                bCourse.teacherPhoto = vCourseData.teacherPhoto
                bCourse.isPopular = vCourseData.isPopular
                bCourse.isTrending = vCourseData.isTrending
                break
            }
        }
        loadCoursesToTable(gCoursesDB.courses)
        $('#edit-course-modal').modal('hide')
    }
}


// Hàm khi bấm nút delete course
function onBtnDeleteCourseClick() {
    // Lấy id của khóa học
    var vTable = $('#courses-table').DataTable()
    var vRowData = vTable.row($(this).parents('tr')).data()
    $('#btn-confirm-delete-course').data('id', vRowData.id)
    $('#delete-course-modal').modal()
}

// Hàm khi nhấn nút confirm delete course
function onBtnConfirmDeleteCourseClick() {
    var vDeleteCourseId = $(this).data('id')
    // Xử lí xóa course data theo id
    for (var bIndex in gCoursesDB.courses) {
        if(gCoursesDB.courses[bIndex].id == vDeleteCourseId) {
            gCoursesDB.courses.splice(bIndex, 1)
            break
        }
    }
    loadCoursesToTable(gCoursesDB.courses)
    $('#delete-course-modal').modal('hide')
}

// Hàm đổ dữ liệu vào bảng
function loadCoursesToTable(paramUserData) {
    var vTable = $('#courses-table').DataTable()
    vTable.clear()
    vTable.rows.add(paramUserData)
    vTable.draw()
}

// Hàm tạo id tự tăng
function getNextId() {
    return gCoursesDB.courses.length ? gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1 : 1
}

// Hàm lấy dữ liệu từ form thêm mới
function getDataFromCreateForm() {
    return {
        id: $('#inp-create-course-id').val(),
        courseCode: $('#inp-create-course-code').val().trim(),
        courseName: $('#inp-create-course-name').val().trim(),
        price: $('#inp-create-price').val().trim(),
        discountPrice: $('#inp-create-discount-price').val().trim(),
        duration: $('#inp-create-duration').val().trim(),
        level: $('#sel-create-level').val(),
        coverImage: $('#inp-create-cover-image').val().trim(),
        teacherName: $('#inp-create-teacher-name').val().trim(),
        teacherPhoto: $('#inp-create-teacher-photo').val().trim(),
        isPopular: $("#chk-create-is-popular").is(':checked'),
        isTrending: $("#chk-create-is-trending").is(':checked'),
    }
}

// Hàm lấy dữ liệu từ form edit
function getDataFormEditCourse() {
    return {
        id: $('#inp-edit-course-id').val(),
        courseCode: $('#inp-edit-course-code').val().trim(),
        courseName: $('#inp-edit-course-name').val().trim(),
        price: $('#inp-edit-price').val().trim(),
        discountPrice: $('#inp-edit-discount-price').val().trim(),
        duration: $('#inp-edit-duration').val().trim(),
        level: $('#sel-edit-level').val(),
        coverImage: $('#inp-edit-cover-image').val().trim(),
        teacherName: $('#inp-edit-teacher-name').val().trim(),
        teacherPhoto: $('#inp-edit-teacher-photo').val().trim(),
        isPopular: $("#chk-edit-is-popular").is(':checked'),
        isTrending: $("#chk-edit-is-trending").is(':checked'),
    }
}

// Hàm clear create form
function clearCreateForm() {
    $('#inp-create-course-id').val('')
    $('#inp-create-course-code').val('')
    $('#inp-create-course-name').val('')
    $('#inp-create-price').val('')
    $('#inp-create-discount-price').val('')
    $('#inp-create-duration').val('')
    $('#sel-create-level').val('')
    $('#inp-create-cover-image').val('')
    $('#inp-create-teacher-name').val('')
    $('#inp-create-teacher-photo').val('')
    $('#chk-create-is-popular').prop('checked', false)
    $('#chk-create-is-trending').prop('checked', false)
}

// Hàm kiểm tra dữ liệu
function validateData(paramCourseData) {
    var vResult = true
    if (!paramCourseData.courseCode) {
        alert('Bạn chưa nhập course code')
        vResult = false
    }
    else if (!paramCourseData.courseName) {
        alert('Bạn chưa nhập course name')
        vResult = false
    }
    else if (!paramCourseData.price || isNaN(paramCourseData.price)) {
        alert('Bạn chưa nhập price và phải là 1 số')
        vResult = false
    }
    else if (!paramCourseData.discountPrice || isNaN(paramCourseData.discountPrice)) {
        alert('Bạn chưa nhập discount price và phải là 1 số')
        vResult = false
    }
    else if (!paramCourseData.duration) {
        alert('Bạn chưa nhập duration')
        vResult = false
    }
    else if (!paramCourseData.level) {
        alert('Bạn chưa nhập level')
        vResult = false
    }
    else if (!paramCourseData.coverImage) {
        alert('Bạn chưa nhập cover image')
        vResult = false
    }
    else if (!paramCourseData.teacherPhoto) {
        alert('Bạn chưa nhập teacher photo')
        vResult = false
    }
    return vResult
}

// Hàm hiển thị course data vào edit table
function loadCourseDataToEditForm(paramCourseData) {
    $('#inp-edit-course-id').val(paramCourseData.id)
    $('#inp-edit-course-code').val(paramCourseData.courseCode)
    $('#inp-edit-course-name').val(paramCourseData.courseName)
    $('#inp-edit-price').val(paramCourseData.price)
    $('#inp-edit-discount-price').val(paramCourseData.discountPrice)
    $('#inp-edit-duration').val(paramCourseData.duration)
    $('#sel-edit-level').val(paramCourseData.level)
    $('#inp-edit-cover-image').val(paramCourseData.coverImage)
    $('#inp-edit-teacher-name').val(paramCourseData.teacherName)
    $('#inp-edit-teacher-photo').val(paramCourseData.teacherPhoto)
    $('#chk-edit-is-popular').prop('checked', paramCourseData.isPopular)
    $('#chk-edit-is-trending').prop('checked', paramCourseData.isTrending)
}